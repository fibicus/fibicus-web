import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { OverviewComponent } from './components/overview/overview.component';
import { EditComponent } from './components/edit/edit.component';
import { DetailComponent } from './components/detail/detail.component';
import { LoginComponent } from './auth/feature/login/login.component';
import { canActivateUsersOnly } from './auth/data-access/auth.guard';
import { SimpleDetailComponent } from './components/simple-detail/simple-detail.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: '', redirectTo: '/main/overview', pathMatch: 'full' },
  {
    path: 'main',
    component: MainComponent,
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'prefix' },
      { path: 'overview', component: OverviewComponent },
      {
        path: 'edit/:id',
        component: EditComponent,
        canActivate: [canActivateUsersOnly],
      },
      { path: 'detail/:id', component: DetailComponent },
      { path: ':id', component: SimpleDetailComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export class DBEntity {
  id?: string;
  deleted?: boolean;
  constructor() {
    this.deleted = false;
  }
}

import { DBEntity } from 'src/app/shared/models/db-entity';
import {
  DocumentData,
  DocumentSnapshot,
  SnapshotOptions,
  Timestamp,
} from '@angular/fire/firestore';

export class Schedule extends DBEntity {
  start: Date;
  title: string;
  isPublic: boolean;
  activeItems: number;

  constructor() {
    super();
    this.title = 'Neuer Flugplan';
    this.start = new Date();
    this.isPublic = false;
    this.activeItems = 0;
  }

  static converter = {
    toFirestore: (schedule: Schedule) => {
      return {
        start: Timestamp.fromDate(schedule.start),
        title: schedule.title,
        isPublic: schedule.isPublic,
        activeItems: schedule.activeItems,
        deleted: schedule.deleted,
      };
    },
    fromFirestore: (
      snapshot: DocumentSnapshot,
      options: SnapshotOptions | undefined
    ) => {
      const data = snapshot.data(options) as DocumentData;
      const item = new Schedule();
      item.id = snapshot.id;
      item.deleted = data['deleted'];
      item.start = data['start'].toDate();
      item.title = data['title'];
      item.isPublic = data['isPublic'];
      item.activeItems = data['activeItems'];
      return item;
    },
  };
}

export interface ItemTime {
  time?: Date; //Not synced to backend, calculated dynamically
  title: string;
  minutes?: number;
  signed: -1 | 1;
}

import { DBEntity } from 'src/app/shared/models/db-entity';
import { ItemStatus } from './item-status';
import { ItemTime } from './item-time';
import {
  DocumentData,
  DocumentSnapshot,
  SnapshotOptions,
  Timestamp,
} from '@angular/fire/firestore';

export class ScheduleItem extends DBEntity {
  title: string;
  description: string;
  planeType: string;
  planeLicensePlate: string;
  pilot: string;
  timeStart: Date;
  duration: number;
  times: ItemTime[];
  // timeStartup: Date;
  // timeLanding: Date;
  // timeTakeoff: Date;
  // timeOnBlock: Date;
  isPublic: boolean;
  saying?: string;
  active?: boolean;

  constructor(initTime: Date) {
    super();
    this.title = 'Programmpunkt';
    this.description = '';
    this.planeType = 'SomePlane';
    this.planeLicensePlate = 'D-ABCD';
    this.pilot = 'Der Pilot';
    this.timeStart = new Date(initTime);
    this.duration = 10;
    this.times = [];
    this.isPublic = false;
  }

  static converter = {
    toFirestore: (scheduleItem: ScheduleItem) => {
      return {
        timeStart: Timestamp.fromDate(scheduleItem.timeStart),
        title: scheduleItem.title,
        description: scheduleItem.description,
        planeType: scheduleItem.planeType,
        planeLicensePlate: scheduleItem.planeLicensePlate,
        pilot: scheduleItem.pilot,
        duration: scheduleItem.duration,
        times: scheduleItem.times.map((t) => ({
          title: t.title,
          signed: t.signed,
          minutes: t.minutes,
        })),
        isPublic: scheduleItem.isPublic,
        deleted: scheduleItem.deleted,
      };
    },
    fromFirestore: (
      snapshot: DocumentSnapshot,
      options: SnapshotOptions | undefined
    ) => {
      const data = snapshot.data(options) as DocumentData;
      const item = new ScheduleItem(data['timeStart'].toDate());
      item.id = snapshot.id;
      item.deleted = data['deleted'];
      item.title = data['title'];
      item.description = data['description'];
      item.planeType = data['planeType'];
      item.planeLicensePlate = data['planeLicensePlate'];
      item.pilot = data['pilot'];
      item.duration = data['duration'];
      item.times = data['times'].map((t: ItemTime) => ({
        title: t.title,
        signed: t.signed,
        minutes: t.minutes,
      }));
      item.isPublic = data['isPublic'];
      return item;
    },
  };
}

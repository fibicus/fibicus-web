export interface ItemStatus {
  order: number;
  name: string;
  showPublic?: boolean;
}

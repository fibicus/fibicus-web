import { Injectable } from '@angular/core';
import {
  Firestore,
  QueryConstraint,
  addDoc,
  collection,
  collectionData,
  doc,
  getDoc,
  getDocs,
  onSnapshot,
  query,
  setDoc,
} from '@angular/fire/firestore';
import { DBEntity } from './models/db-entity';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export abstract class GenericService<T extends DBEntity> {
  protected abstract collectionName: string;
  constructor(protected firestore: Firestore) {}

  public async get(firestoreQuery: QueryConstraint[] = []) {
    const q = query(
      collection(this.firestore, this.collectionName),
      ...firestoreQuery
    );
    const querySnapshot = await getDocs(q);
    return querySnapshot.docs.map(
      (doc) => Object.assign(doc.data(), { id: doc.id }) as T
    );
  }

  public async getOne(id: string): Promise<T | undefined> {
    const foundDoc = await getDoc(doc(this.firestore, this.collectionName, id));
    if (!foundDoc.exists()) {
      return undefined;
    }
    return Object.assign(foundDoc.data(), { id: foundDoc.id }) as T;
  }

  public async create(entity: Omit<T, 'id'>): Promise<string> {
    const newDoc = await addDoc(
      collection(this.firestore, this.collectionName),
      entity
    );
    return newDoc.id;
  }

  public async createWithId(
    entity: Omit<T, 'id'>,
    id: string
  ): Promise<string> {
    'Create with Id';
    await setDoc(doc(this.firestore, this.collectionName, id), entity);
    return id;
  }

  public async update(entity: T) {
    if (!entity.id) throw new Error('Entity has no id');
    const fbEntity: Omit<T, 'id'> & { id?: string } = { ...entity };
    delete fbEntity.id;
    const updatedEntity = await setDoc(
      doc(this.firestore, this.collectionName, entity.id),
      fbEntity
    );
    return entity.id;
  }

  public delete(id: string) {
    return setDoc(
      doc(this.firestore, this.collectionName, id),
      { deleted: true },
      {
        merge: true,
      }
    );
  }

  public get$(firestoreQuery: QueryConstraint[] = []): Observable<T[]> {
    const q = query(
      collection(this.firestore, this.collectionName),
      ...firestoreQuery
    );
    return collectionData(q, { idField: 'id' }) as Observable<T[]>;
  }

  public getOne$(id: string): Observable<T> {
    return new Observable((observer) => {
      return onSnapshot(doc(this.firestore, this.collectionName, id), (doc) => {
        if (doc.exists())
          observer.next(Object.assign(doc.data(), { id: doc.id }) as T);
      });
    });
  }
}

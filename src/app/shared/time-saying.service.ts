import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TimeSayingService {
  getSaying(fromTime: Date, toTime: Date): string {
    const timeDelta = toTime.getTime() - fromTime.getTime();
    const hourDelta = Math.floor(timeDelta / 1000 / 60 / 60);
    const minuteDelta =
      Math.floor(timeDelta / 1000 / 60) -
      Math.floor(timeDelta / 1000 / 60 / 60) * 60;
    // let secondDelta =
    //   Math.floor(timeDelta / 1000) -
    //   Math.floor(timeDelta / 1000 / 60) * 60;
    let timeDeltaString = 'Noch ';
    if (timeDelta < 0) {
      return 'Leider schon vorbei';
    }

    if (hourDelta > 0) {
      timeDeltaString = timeDeltaString + hourDelta + 'h ';
    }
    if (minuteDelta > 0) {
      timeDeltaString = timeDeltaString + minuteDelta + 'min ';
    }
    // if (secondDelta > 0) {
    //   timeDeltaString = timeDeltaString + secondDelta + 's ';
    // }

    return timeDeltaString;
  }
}

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, inject } from '@angular/core';
import {
  CollectionReference,
  Firestore,
  addDoc,
  collection,
  collectionData,
  deleteDoc,
  doc,
  onSnapshot,
  setDoc,
} from '@angular/fire/firestore';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ActivatedRoute } from '@angular/router';
import { RxState } from '@rx-angular/state';
import { RxEffects } from '@rx-angular/state/effects';
import { Observable, map, tap } from 'rxjs';
import { Schedule } from 'src/app/shared/models/schedule';
import { ScheduleItem } from 'src/app/shared/models/schedule-item';
import { DialogAddTimeComponent } from '../dialog-add-time/dialog-add-time.component';
import { MatDialog } from '@angular/material/dialog';
import { ItemTime } from 'src/app/shared/models/item-time';

@Component({
  selector: 'fib-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [RxState],
})
export class EditComponent {
  protected schedule$: Observable<Schedule>;
  protected scheduleItems$: Observable<ScheduleItem[]>;
  private firestore: Firestore = inject(Firestore);
  openPanels: { [key: string]: boolean } = {};
  scheduleId = '';
  itemsCollection!: CollectionReference<ScheduleItem>;

  constructor(
    private route: ActivatedRoute,
    public itemDialog: MatDialog,
    private state: RxState<{
      schedule: Schedule;
      scheduleItems: ScheduleItem[];
    }>
  ) {
    this.schedule$ = this.state.select('schedule');

    this.scheduleItems$ = this.state.select('scheduleItems');
  }

  ngOnInit() {
    this.scheduleId = this.route.snapshot.params['id'];
    this.itemsCollection = collection(
      this.firestore,
      'schedules',
      this.scheduleId,
      'items'
    ).withConverter(ScheduleItem.converter);
    this.state.connect(
      'schedule',
      new Observable<Schedule>((observer) =>
        onSnapshot(
          doc(this.firestore, 'schedules', this.scheduleId).withConverter(
            Schedule.converter
          ),
          (doc) => {
            observer.next(doc.data());
          }
        )
      )
    );
    this.state.connect(
      'scheduleItems',
      collectionData(this.itemsCollection, {
        idField: 'id',
      }).pipe(
        map((is) => {
          return is
            .sort((a, b) => a.timeStart.getTime() - b.timeStart.getTime())
            .map((i) => {
              i.times = i.times.map((t) => ({
                ...t,
                time: new Date(
                  i.timeStart.getTime() + (t.minutes || 0) * 60000 * t.signed
                ),
              }));
              return i;
            });
        })
      ) as Observable<ScheduleItem[]>
    );
  }

  protected async updateSchedule(partialSchedule: Partial<Schedule>) {
    return setDoc(
      doc(this.firestore, 'schedules', this.scheduleId),
      partialSchedule,
      { merge: true }
    );
  }

  async triggerRecalculate(): Promise<void> {
    return this.calculateTimes(
      this.state.get('schedule'),
      this.state.get('scheduleItems')
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    const items = this.state.get('scheduleItems');
    moveItemInArray(items, event.previousIndex, event.currentIndex);
    this.calculateTimes(this.state.get('schedule'), items);
  }

  setDate(event: MatDatepickerInputEvent<Date>): void {
    const date = this.state.get('schedule').start;
    date.setFullYear(
      event.value?.getFullYear() || 1970,
      event.value?.getMonth() || 1,
      event.value?.getDate() || 1
    );
    this.updateSchedule({ start: date });
  }

  async saveItems(items: ScheduleItem[]): Promise<void> {
    for (const item of items) {
      setDoc(doc(this.itemsCollection, item.id), item);
    }
  }

  calculateTimes(schedule: Schedule, items: ScheduleItem[]): void {
    let comulatedDuration = 0;
    for (const item of items) {
      item.timeStart.setTime(schedule.start.getTime() + comulatedDuration);
      if (item.isPublic) {
        comulatedDuration += item.duration * 60000;
      }
    }
    this.saveItems(items);
  }

  setItemTimeDuration(item: ScheduleItem, timeItem: ItemTime, $event: number) {
    const foundItem = item.times.find((t) => t === timeItem);
    if (foundItem) {
      foundItem.minutes = $event;
      this.saveItems([item]);
    }
  }

  async setItem(item: ScheduleItem, key: string) {
    if (['isPublic', 'duration'].includes(key)) {
      await this.triggerRecalculate();
    } else {
      this.saveItems([item]);
    }
  }

  addItem(): void {
    const item = new ScheduleItem(this.state.get('schedule').start);
    addDoc(this.itemsCollection, item);
    this.updateSchedule({
      activeItems: this.state.get('schedule').activeItems + 1,
    });
  }

  async setScheduleStart(timString: string): Promise<void> {
    const date = this.state.get('schedule').start;
    date.setHours(
      parseInt(timString.slice(0, 2), 10),
      parseInt(timString.slice(3, 5), 10)
    );
    this.state.set('schedule', (os) => ({ ...os.schedule, start: date }));
    await this.triggerRecalculate();
    this.updateSchedule({ start: date });
  }

  openAddTime(scheduleItem: ScheduleItem): void {
    const dialogRef = this.itemDialog.open(DialogAddTimeComponent, {
      width: '250px',
      data: {
        item: {
          title: '',
          signed: 1,
        } as ItemTime,
      },
    });
    dialogRef.afterClosed().subscribe((result: ItemTime) => {
      if (result && result.title && result.signed && result.minutes) {
        if (!scheduleItem.times) {
          scheduleItem.times = [];
        }
        scheduleItem.times.push(result);
        this.saveItems([scheduleItem]);
      }
    });
  }

  removeItem(item: ScheduleItem): void {
    deleteDoc(doc(this.itemsCollection, item.id));
    if (item.isPublic) {
      this.updateSchedule({
        activeItems: this.state.get('schedule').activeItems - 1,
      });
    }
  }

  deleteItemTime(item: ScheduleItem, index: number): void {
    item.times.splice(index, 1);
    this.saveItems([item]);
  }
}

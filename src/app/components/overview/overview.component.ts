import { Component, inject } from '@angular/core';
import {
  Firestore,
  QueryConstraint,
  collection,
  collectionData,
  query,
  where,
} from '@angular/fire/firestore';
import { RxState } from '@rx-angular/state';
import { Observable, map, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/data-access/auth.service';
import { User } from 'src/app/auth/data-access/models/user';
import { Schedule } from 'src/app/shared/models/schedule';

@Component({
  selector: 'fib-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  providers: [RxState],
})
export class OverviewComponent {
  protected schedules$: Observable<Schedule[]>;
  private firestore: Firestore = inject(Firestore);
  canSeeExtended$: Observable<boolean>;
  canEdit$: Observable<boolean>;

  constructor(
    private authService: AuthService,
    private state: RxState<{ schedules: Schedule[]; user: User | null }>
  ) {
    this.state.connect('user', this.authService.currentUser$);
    this.canSeeExtended$ = this.state
      .select('user')
      .pipe(map((user) => !!user?.canSeeExtended));
    this.canEdit$ = this.state
      .select('user')
      .pipe(map((user) => !!user?.canEdit));
    this.state.connect(
      'schedules',
      this.canEdit$.pipe(
        switchMap((canEdit) => {
          let q = query(
            collection(this.firestore, 'schedules').withConverter(
              Schedule.converter
            ),
            where('isPublic', '==', true)
          );
          if (canEdit) {
            q = query(
              collection(this.firestore, 'schedules').withConverter(
                Schedule.converter
              )
            );
          }
          return collectionData(q, {
            idField: 'id',
          }) as Observable<Schedule[]>;
        })
      )
    );
    this.schedules$ = this.state.select('schedules');
  }
}

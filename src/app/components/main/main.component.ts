import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/data-access/auth.service';

@Component({
  selector: 'fib-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {
  constructor(protected authService: AuthService) {}
}

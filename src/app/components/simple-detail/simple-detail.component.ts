import { Component, OnInit, inject } from '@angular/core';
import {
  CollectionReference,
  Firestore,
  collection,
  collectionData,
  doc,
  onSnapshot,
  query,
  where,
} from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { RxState } from '@rx-angular/state';
import { Observable, combineLatest, map, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/data-access/auth.service';
import { Schedule } from 'src/app/shared/models/schedule';
import { ScheduleItem } from 'src/app/shared/models/schedule-item';

@Component({
  selector: 'fib-simple-detail',
  templateUrl: './simple-detail.component.html',
  styleUrls: ['./simple-detail.component.scss'],
  providers: [RxState],
})
export class SimpleDetailComponent implements OnInit {
  protected schedule$: Observable<Schedule>;
  protected scheduleItems$: Observable<ScheduleItem[]>;
  private firestore: Firestore = inject(Firestore);
  private itemsCollection!: CollectionReference<ScheduleItem>;
  scheduleId = '';

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private state: RxState<{
      schedule: Schedule;
      scheduleItems: ScheduleItem[];
    }>
  ) {
    this.schedule$ = this.state.select('schedule').pipe(
      map((s) => {
        s.start.setFullYear(1970, 1, 1);
        return s;
      })
    );

    this.scheduleItems$ = combineLatest([
      this.state.select('scheduleItems'),
    ]).pipe(
      map(([items]) => {
        return items.map((i) => {
          i.timeStart.setFullYear(1970, 1, 1);
          i.times.forEach(
            (t) =>
              (t.time = new Date(
                new Date(i.timeStart).setMinutes(
                  i.timeStart.getMinutes() + (t.minutes || 0) * t.signed
                )
              ))
          );
          return i;
        });
      })
    );
  }

  ngOnInit() {
    this.scheduleId = this.route.snapshot.params['id'];
    this.itemsCollection = collection(
      this.firestore,
      'schedules',
      this.scheduleId,
      'items'
    ).withConverter(ScheduleItem.converter);

    this.state.connect(
      'schedule',
      new Observable<Schedule>((observer) =>
        onSnapshot(
          doc(this.firestore, 'schedules', this.scheduleId).withConverter(
            Schedule.converter
          ),
          (doc) => {
            observer.next(doc.data());
          }
        )
      )
    );

    this.state.connect(
      'scheduleItems',
      collectionData(
        query(this.itemsCollection, where('isPublic', '==', true)),
        {
          idField: 'id',
        }
      ) as Observable<ScheduleItem[]>
    );
  }

  getNumber(date: Date | undefined): number {
    if (!date) return 0;
    const num = date.getTime();
    return Math.floor(num / 10000);
  }
}

import { Component, OnInit, inject } from '@angular/core';
import {
  CollectionReference,
  Firestore,
  collection,
  collectionData,
  doc,
  onSnapshot,
  query,
  where,
} from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { RxState } from '@rx-angular/state';
import { RxEffects } from '@rx-angular/state/effects';
import { Observable, combineLatest, interval, map, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/data-access/auth.service';
import { Schedule } from 'src/app/shared/models/schedule';
import { ScheduleItem } from 'src/app/shared/models/schedule-item';
import { TimeSayingService } from 'src/app/shared/time-saying.service';

@Component({
  selector: 'fib-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [RxState, RxEffects],
})
export class DetailComponent implements OnInit {
  protected schedule$: Observable<Schedule>;
  protected scheduleItems$: Observable<ScheduleItem[]>;
  private firestore: Firestore = inject(Firestore);
  private itemsCollection!: CollectionReference<ScheduleItem>;
  protected time$ = interval(1000).pipe(
    map(() => new Date(new Date().setFullYear(1970, 1, 1)))
  );
  hidePassed = false;
  extended = false;
  scheduleId = '';
  canSeeExtended = false;
  canEdit$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    //   private scheduleService: ScheduleService,
    //   private location: Location,
    private sayingService: TimeSayingService,
    private authService: AuthService,
    private state: RxState<{
      schedule: Schedule;
      scheduleItems: ScheduleItem[];
    }>,
    private effects: RxEffects
  ) {
    this.schedule$ = this.state.select('schedule').pipe(
      map((s) => {
        s.start.setFullYear(1970, 1, 1);
        return s;
      })
    );

    this.canEdit$ = this.authService.currentUser$.pipe(
      map((user) => !!user?.canEdit)
    );

    this.scheduleItems$ = combineLatest([
      this.state.select('scheduleItems'),
      this.time$,
    ]).pipe(
      map(([items, time]) => {
        return items
          .filter((i) => {
            return this.hidePassed
              ? i.timeStart.getTime() > time.getTime()
              : true;
          })
          .map((i) => {
            // i.active = i.timeStart.getTime() <= time.getTime();

            i.timeStart.setFullYear(1970, 1, 1);
            i.times.forEach(
              (t) =>
                (t.time = new Date(
                  new Date(i.timeStart).setMinutes(
                    i.timeStart.getMinutes() + (t.minutes || 0) * t.signed
                  )
                ))
            );
            i.saying = this.sayingService.getSaying(time, i.timeStart);
            return i;
          });
      })
    );
    this.effects.register(this.authService.currentUser$, (user) => {
      this.canSeeExtended = !!user?.canSeeExtended;
      this.extended =
        this.canSeeExtended &&
        this.route.snapshot.queryParamMap.get('extended') === 'true';
    });
  }

  ngOnInit() {
    this.scheduleId = this.route.snapshot.params['id'];
    this.itemsCollection = collection(
      this.firestore,
      'schedules',
      this.scheduleId,
      'items'
    ).withConverter(ScheduleItem.converter);

    this.state.connect(
      'schedule',
      new Observable<Schedule>((observer) =>
        onSnapshot(
          doc(this.firestore, 'schedules', this.scheduleId).withConverter(
            Schedule.converter
          ),
          (doc) => {
            observer.next(doc.data());
          }
        )
      )
    );

    this.state.connect(
      'scheduleItems',
      this.canEdit$.pipe(
        switchMap((canEdit) => {
          let q = query(this.itemsCollection, where('isPublic', '==', true));
          if (canEdit) {
            q = query(this.itemsCollection);
          }
          return collectionData(q, {
            idField: 'id',
          }) as Observable<ScheduleItem[]>;
        })
      )
    );

    this.hidePassed =
      this.route.snapshot.queryParamMap.get('hidePassed') === 'true';
  }

  getNumber(date: Date | undefined): number {
    if (!date) return 0;
    const num = date.getTime();
    return Math.floor(num / 10000);
  }
}

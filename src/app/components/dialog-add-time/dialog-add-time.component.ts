import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ItemTime } from 'src/app/shared/models/item-time';

@Component({
  selector: 'fib-dialog-add-time',
  templateUrl: './dialog-add-time.component.html',
  styleUrls: ['./dialog-add-time.component.scss'],
})
export class DialogAddTimeComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogAddTimeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ItemTime
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../data-access/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'fib-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  protected password = '';
  protected username = '';
  protected authenticated = false;

  loginError = false;
  constructor(private authService: AuthService, private router: Router) {}

  login(): void {
    this.authService
      .signIn(this.username, this.password)
      .then(() => {
        this.router.navigate(['/']);
        console.log('Logged in');
      })
      .catch(() => {
        this.loginError = true;
      });
  }

  register(): void {
    this.authService
      .register(this.username, this.password)
      .then(() => {
        this.router.navigate(['/']);
        console.log('Logged in');
      })
      .catch((err) => {
        console.error('Error while registering', err);
        this.loginError = true;
      });
  }
}

import { Injectable } from '@angular/core';
import {
  Auth,
  authState,
  createUserWithEmailAndPassword,
  sendEmailVerification,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signInWithPopup,
} from '@angular/fire/auth';
import { User as FirebaseUser } from '@angular/fire/auth';
import { UserService } from './user.service';
import { User } from './models/user';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  get currentUser$(): Observable<(User & FirebaseUser) | null> {
    return this._currentUser$.asObservable();
  }
  private _currentUser$: BehaviorSubject<(User & FirebaseUser) | null> =
    new BehaviorSubject<(User & FirebaseUser) | null>(null);
  constructor(private auth: Auth, private userService: UserService) {
    authState(auth).subscribe(async (user: FirebaseUser | null) => {
      // Get user data and save it
      if (!user) {
        this._currentUser$.next(null);
        return;
      }
      this._currentUser$.next(
        Object.assign(user, await this.userService.getOne(user.uid)) || null
      );
    });
  }

  public signIn(email: string, password: string) {
    return signInWithEmailAndPassword(this.auth, email, password).then(
      () => {
        return true;
      },
      (error) => {
        throw Error(error);
      }
    );
  }

  public register(email: string, password: string) {
    return createUserWithEmailAndPassword(this.auth, email, password).then(
      async (result) => {
        this.sendVerificationEmail();
        const newUser = {
          canEdit: false,
          canSeeExtended: false,
          deleted: false,
        };
        await this.userService.createWithId(newUser, result.user.uid);
        this._currentUser$.next(
          Object.assign(result.user, newUser, {
            id: result.user.uid,
          })
        );
      },
      (error) => {
        console.error(error);
        throw Error(error);
      }
    );
  }

  public sendVerificationEmail() {
    if (this.auth.currentUser) {
      sendEmailVerification(this.auth.currentUser).then(() => {
        //ToDo: Navigate to info page
      });
    } else {
      throw Error('No user logged in');
    }
  }

  public resetPassword(email: string) {
    sendPasswordResetEmail(this.auth, email).then(() => {
      window.alert('Password reset email sent, check your inbox.');
      // navigate to login
    });
  }

  public signOut() {
    this.auth.signOut().then(() => {
      // Clear user
      // Navigate to login page
      this._currentUser$.next(null);
    });
  }

  public authLogin(provider: any) {
    return signInWithPopup(this.auth, provider).then(async (result) => {
      // Send User Data to Firestore
      this.sendVerificationEmail();
      const newUser: User = {
        id: result.user.uid,
        canEdit: false,
        canSeeExtended: false,
        deleted: false,
      };
      await this.userService.create(newUser);
      this._currentUser$.next(Object.assign(result.user, newUser));
    });
  }
}

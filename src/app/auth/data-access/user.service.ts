import { Injectable } from '@angular/core';
import { GenericService } from 'src/app/shared/generic.service';
import { User } from './models/user';
import { doc, setDoc } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class UserService extends GenericService<User> {
  collectionName = 'users';
}

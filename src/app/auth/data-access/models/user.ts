import { DBEntity } from 'src/app/shared/models/db-entity';

export interface User extends DBEntity {
  canSeeExtended: boolean;
  canEdit: boolean;
}

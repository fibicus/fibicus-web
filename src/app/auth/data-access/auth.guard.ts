import { Injectable, inject } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { CanActivateFn } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  constructor(private auth: Auth) {}
  isLoggedIn(): boolean {
    const firebaseUser = this.auth.currentUser;
    return !!firebaseUser;
  }
}

export const canActivateUsersOnly: CanActivateFn = () => {
  return inject(AuthGuard).isLoggedIn();
};

// const canActivateEditorsOnly: CanActivateFn = () => {
//   return inject(AuthGuard).isLoggedIn();
// };

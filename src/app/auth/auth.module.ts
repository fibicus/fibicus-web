import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './feature/login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

// const routes: Routes = [
//   { path: '', redirectTo: 'login', pathMatch: 'full' },
//   {
//     path: 'login',
//     component: LoginComponent,
//   },
// ];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    // RouterModule.forChild(routes),
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
  ],
})
export class AuthModule {}
